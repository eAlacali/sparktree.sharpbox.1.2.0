﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using AppLimit.CloudComputing.SharpBox.Exceptions;
using AppLimit.CloudComputing.SharpBox.StorageProvider.API;
using AppLimit.CloudComputing.SharpBox.StorageProvider;
using AppLimit.CloudComputing.SharpBox.StorageProvider.BaseObjects;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Resources;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Response;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Logic
{
    /// <summary>
    /// This derived class is a special implementation of the StorageProvider services 
    /// interface which is responsable for all interaction with the remote service. The mock 
    /// service works as follows:
    /// - Virtual FileSystem is /
    ///     - Data
    ///         - TestFolder1
    ///         - TestFolder2
    ///     - Date2
    ///         - TestFolder3
    ///             - File.rnd
    ///
    /// - It's possible to upload data and create diretories, just in memory
    /// 
    /// </summary>
    internal class GoogleDriveStorageProviderService : GenericStorageProviderService
    {

        // token storage definitions        
        public const String TokenDriveCredUsername = "TokenDriveUsername";
        public const String TokenDriveCredPassword = "TokenDrivePassword";
        public const String TokenDriveAppKey = "TokenDriveAppKey";
        public const String TokenDriveAppSecret = "TokenDriveAppSecret";


        // server and base url 
        public const string DrivePrototcolPrefix = "https://";
        public const String DriveServer = "www.googleapis.com/drive";
        public const String DriveBaseUrl = DrivePrototcolPrefix + DriveServer;
        public const String DriveApiVersion = "v2";

        // action urls

        /// <summary>
        /// GET - Gets the information about the current user along with Drive API settings
        /// </summary>
        public const String DriveGetAccountInfo = DriveBaseUrl + "/" + DriveApiVersion + "/about";

        //Files
        /// <summary>
        /// GET - Gets a file's metadata by ID.
        /// </summary>
        public const String DriveFileMetadata = DriveBaseUrl + "/" + DriveApiVersion + "/files/{0}";
        /// <summary>
        /// DELETE - Permanently deletes a file by ID. Skips the trash.
        /// </summary>
        public const String DriveDeleteFile = DriveBaseUrl + "/" + DriveApiVersion + "/files/{0}";
        /// <summary>
        /// PUT - Updates file metadata and/or content.
        /// </summary>
        public const String DriveUpdateFile = DriveBaseUrl + "/" + DriveApiVersion + "/files";
        /// <summary>
        /// GET - Lists the user's files.
        /// </summary>
        public const String DriveListFiles = DriveBaseUrl + "/" + DriveApiVersion + "/files";

        //Children
        /// <summary>
        /// DELETE - Removes a child from a folder. 
        /// {0} folderId , {1} childId
        /// </summary>
        public const String DriveRemoveChild = DriveBaseUrl + "/" + DriveApiVersion + "/files/{0}/children/{1}";

        /// <summary>
        /// POST - Inserts a file into a folder.
        /// {0} folderId
        /// </summary>
        public const String DriveInsertChild = DriveBaseUrl + "/" + DriveApiVersion + "/files/{0}/children";

        /// <summary>
        /// GET - Gets a specific child reference.
        /// {0} folderId , {1} childId
        /// </summary>
        public const String DriveGetChild = DriveBaseUrl + "/" + DriveApiVersion + "/files/{0}/children/{1}";

        /// <summary>
        /// GET - Lists a folder's children.  To list all children of the root folder, use the alias root for the folderId value.
        /// {0} folderId
        /// </summary>
        public const String DriveListChildren = DriveBaseUrl + "/" + DriveApiVersion + "/files/{0}/children";

        //Parents
        /// <summary>
        /// DELETE  - Removes a parent from a file.
        /// {0} fileId, {1} parentId
        /// </summary>
        public const String DriveRemoveParent = DriveBaseUrl + "/" + DriveApiVersion + "/files/{0}/parents/{1}";

        /// <summary>
        /// POST - Adds a parent folder for a file.
        /// </summary>
        public const String DriveAddParent = DriveBaseUrl + "/" + DriveApiVersion + "/files/{0}/parents";


        #region IStorageProviderService Members
        /// <summary>
        /// Verify credentialtype has to be implemented to verify if the given credentials are
        /// compatible with this service! Our MockProvider only accepts MockProviderCredentials.
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public override bool VerifyAccessTokenType(ICloudStorageAccessToken token)
        {
            if (token is GoogleDriveTokenStorage)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The CreateSession method has to establish an authenticated connection with the cloud 
        /// storage service and returns an instance of a type which implements the 
        /// IStorageProviderSession interface. The MockProvider just returns an instance of the
        /// MockProviderSession classe.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public override IStorageProviderSession CreateSession(ICloudStorageAccessToken token, ICloudStorageConfiguration configuration)
        {

            // return the session {Real session handlers will store the configuration and the created accesstoken in the session)
            return new GoogleDriveStorageProviderSession(token as GoogleDriveTokenStorage, this, configuration as GoogleDriveProviderConfiguration);
        }

        /// <summary>
        /// The request resource interface is reponsable to communicate with the cloud storage service to
        /// download all meta data of a specific resource which is adressed via Name and parent container.
        /// The mock provider works only in memory so only the existing childs will be returned.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="Name">the id of the file/folder being requested</param>
        /// <param name="parent"></param>
        /// <returns></returns>
        /// <exception cref="SharpBoxException"></exception>
        public override ICloudFileSystemEntry RequestResource(IStorageProviderSession session, string Name, ICloudDirectoryEntry parent)
        {

            //TODO: figure out what to do when the parent is provided.
            // build url

            // request the data from url           
            int code;
            WebRequest requestItem = OAuth2.OAuth2Service.GetWebRequest(String.Format(DriveFileMetadata, Name), "GET", session.SessionToken as GoogleDriveTokenStorage, null);

            FileModel item = OAuth2.OAuth2Service.ExecuteWebRequest<FileModel>(requestItem, out code);

            if (code != (int)HttpStatusCode.OK)
            {
                HttpException hex = new HttpException(Convert.ToInt32(code), "HTTP Error");

                throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotRetrieveDirectoryList, hex);
            }
            else
            {
                ICloudFileSystemEntry directoryEntry = GoogleDriveRequestParser.CreateObjectsFromFileModel(item, this,
                                                                                                        session);
                return directoryEntry;
            }

            



            //// build the entry and subchilds
            //BaseFileEntry entry = DropBoxRequestParser.CreateObjectsFromJsonString(res, this, session);

            //// check if it was a deleted file
            //if (entry.IsDeleted)
            //    return null;

            //// set the parent
            //entry.Parent = parent;

            //// go ahead
            //return entry;   
        }

        /// <summary>
        /// This method is reponsable for refreshing an resource in the memory meta data cache against the 
        /// real existing resources on the cloud storage provider. The Mock Provider is working on memory 
        /// only, so nothing todo
        /// </summary>
        /// <param name="session"></param>
        /// <param name="resource"></param>
        public override void RefreshResource(IStorageProviderSession session, ICloudFileSystemEntry resource)
        {
            // request the data from url           
            int code;
            WebRequest requestItem = OAuth2.OAuth2Service.GetWebRequest(
                String.Format(DriveFileMetadata, resource.Name), "GET", session.SessionToken as GoogleDriveTokenStorage,
                null);

            FileModel item = OAuth2.OAuth2Service.ExecuteWebRequest<FileModel>(requestItem, out code);

            if (code != (int) HttpStatusCode.OK)
            {
                HttpException hex = new HttpException(Convert.ToInt32(code), "HTTP Error");

                throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotRetrieveDirectoryList, hex);
            }
            else
            {
                ICloudFileSystemEntry directoryEntry = GoogleDriveRequestParser.UpdateObjectFromFileModel(item, resource, this,
                                                                                                          session);
                return;

            }
        }

        /// <summary>
        /// This method is called from the runtime when a resource has to be created in the cloud storage 
        /// provider. This means not that data will be uploaded, only the meta data information will be 
        /// generated. In our example only memory operations are implemented so we create the resource in 
        /// our memory model.*-
        /// </summary>
        /// <param name="session"></param>
        /// <param name="Name"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public override ICloudFileSystemEntry CreateResource(IStorageProviderSession session, string Name, ICloudDirectoryEntry parent)
        {
            return GenericStorageProviderFactory.CreateDirectoryEntry(session, Name, parent);
        }

        /// <summary>
        /// This method is called when a specific resource has to be removed from the cloud storage service. 
        /// The Mock Provider only implements in memory actions so we remove the resource only from our virtual 
        /// filesystem in memory
        /// </summary>
        /// <param name="session"></param>
        /// <param name="entry"></param>
        /// <returns></returns>
        public override bool DeleteResource(IStorageProviderSession session, ICloudFileSystemEntry entry)
        {
            // remove from memory tree
            GenericStorageProviderFactory.DeleteFileSystemEntry(session, entry);

            // go ahead
            return true;
        }

        /// <summary>
        /// This method is called from the runtime if a resource has to be removed from his current 
        /// parent under a new parent. The MockProvider has to do the move operation only in his 
        /// in memory cache.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="fsentry"></param>
        /// <param name="newParent"></param>
        /// <returns></returns>
        public override bool MoveResource(IStorageProviderSession session, ICloudFileSystemEntry fsentry, ICloudDirectoryEntry newParent)
        {
            // move the entry
            GenericStorageProviderFactory.MoveFileSystemEntry(session, fsentry, newParent);

            // go ahead
            return true;
        }

        /// <summary>
        /// This method is called to establish a download stream. In real world scenarios the stream will be a
        /// WebREquest based stream which needs to execute some task during disposal. 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="fileSystemEntry"></param>
        /// <returns></returns>
        public override System.IO.Stream CreateDownloadStream(IStorageProviderSession session, ICloudFileSystemEntry fileSystemEntry)
        {
            //// cast the sesion
            //MockProviderSession ses = (MockProviderSession)session;

            //// receive the data stream from session 
            //return ses.GetDataStream(fileSystemEntry);
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will be called to commit a generated stream operation. In our example only the filesize 
        /// length has to be setted as post operations scenario. 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="fileSystemEntry"></param>
        /// <param name="Direction"></param>
        /// <param name="NotDisposedStream"></param>
        public override void CommitStreamOperation(IStorageProviderSession session, ICloudFileSystemEntry fileSystemEntry, nTransferDirection Direction, Stream NotDisposedStream)
        {
            if (Direction == nTransferDirection.nUpload)
                GenericStorageProviderFactory.ModifyFileSystemEntryLength(fileSystemEntry, NotDisposedStream.Length);
        }

        /// <summary>
        /// This method is called to establish an upload stream. In real world scenarios the stream will be a
        /// WebREquest based stream which needs to execute some task during disposal. 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="fileSystemEntry"></param>
        /// <param name="uploadSize"></param>
        /// <returns></returns>
        public override System.IO.Stream CreateUploadStream(IStorageProviderSession session, ICloudFileSystemEntry fileSystemEntry, long uploadSize)
        {
            //// cast the sesion
            //MockProviderSession ses = (MockProviderSession)session;

            //// receive the data stream from session 
            //return ses.GetDataStream(fileSystemEntry);
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method is called if a resource has to be renamed in the cloud storage service. Our 
        /// MockProvider needs only to rename the resource in memory
        /// </summary>
        /// <param name="session"></param>
        /// <param name="fsentry"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        public override bool RenameResource(IStorageProviderSession session, ICloudFileSystemEntry fsentry, string newName)
        {
            // rename in memory
            GenericStorageProviderFactory.RenameFileSystemEntry(session, fsentry, newName);

            // go ahead
            return true;
        }

        #endregion
        #region Authorization Helper

        //private GoogleDriveStorageProviderSession Authorize(GoogleDriveAccessToken token, GoogleDriveProviderConfiguration configuration)
        //{
        //    // Get a valid dropbox session through oAuth authorization
        //    GoogleDriveStorageProviderSession session = BuildSessionFromAccessToken(token, configuration);

        //    //( Get a valid root object
        //    return GetRootBySessionExceptionHandled(session);
        //}

        //private GoogleDriveStorageProviderSession GetRootBySessionExceptionHandled(GoogleDriveStorageProviderSession session)
        //{
        //    try
        //    {
        //        ICloudDirectoryEntry root = GetRootBySession(session);

        //        // return the infos
        //        return root == null ? null : session;
        //    }
        //    catch (SharpBoxException ex)
        //    {
        //        // check if the exception an http error 403
        //        if (ex.InnerException is HttpException)
        //        {
        //            if ((((HttpException)ex.InnerException).GetHttpCode() == 403) || (((HttpException)ex.InnerException).GetHttpCode() == 401))
        //                throw new UnauthorizedAccessException();
        //        }

        //        // otherwise rethrow the old exception
        //        throw ex;
        //    }
        //}

        //private ICloudDirectoryEntry GetRootBySession(GoogleDriveStorageProviderSession session)
        //{
        //    // now check if we have application keys and secrets from a sandbox or a fullbox
        //    // try to load the root of the full box if this fails we have only sandbox access
        //    ICloudDirectoryEntry root = null;

        //    try
        //    {
        //        root = RequestResource(session, "/", null) as ICloudDirectoryEntry;
        //    }
        //    catch (SharpBoxException ex)
        //    {
        //        if (session.SandBoxMode)
        //            throw ex;
        //        else
        //        {
        //            // enable sandbox mode
        //            session.SandBoxMode = true;

        //            // retry to get root object
        //            root = RequestResource(session, "/", null) as ICloudDirectoryEntry;
        //        }
        //    }

        //    // go ahead
        //    return root;
        //}

        //public DropBoxStorageProviderSession BuildSessionFromAccessToken(DropBoxToken token, DropBoxConfiguration configuration)
        //{
        //    // build the consumer context
        //    var consumerContext = new OAuthConsumerContext(token.BaseTokenInformation.ConsumerKey, token.BaseTokenInformation.ConsumerSecret);

        //    // build the session
        //    var session = new DropBoxStorageProviderSession(token, configuration, consumerContext, this);

        //    // go aahead
        //    return session;
        //}

        //public static String GetRootToken(DropBoxStorageProviderSession session)
        //{
        //    return session.SandBoxMode ? "sandbox" : "dropbox";
        //}

        //private bool MoveOrRenameItem(DropBoxStorageProviderSession session, BaseFileEntry orgEntry, String toPath)
        //{
        //    return MoveOrRenameOrCopyItem(session, orgEntry, toPath, false);
        //}

        //private bool CopyItem(DropBoxStorageProviderSession session, BaseFileEntry orgEntry, String toPath)
        //{
        //    return MoveOrRenameOrCopyItem(session, orgEntry, toPath, true);
        //}

        //private bool MoveOrRenameOrCopyItem(DropBoxStorageProviderSession session, BaseFileEntry orgEntry, String toPath, bool copy)
        //{
        //    // build the path for resource
        //    String resourcePath = GenericHelper.GetResourcePath(orgEntry);

        //    // request the json object via oauth
        //    var parameters = new Dictionary<string, string>
        //    {
        //        { "from_path", resourcePath },
        //        { "root", GetRootToken(session) },
        //        { "to_path", toPath }
        //    };

        //    try
        //    {
        //        // move or rename the entry
        //        int code;
        //        var res = DropBoxRequestParser.RequestResourceByUrl(GetUrlString(copy ? DropBoxCopyItem : DropBoxMoveItem, session.ServiceConfiguration), parameters, this, session, out code);

        //        // update the entry
        //        DropBoxRequestParser.UpdateObjectFromJsonString(res, orgEntry, this, session);
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //private string GetResourceUrlInternal(IStorageProviderSession session, ICloudFileSystemEntry fileSystemEntry)
        //{
        //    // cast varibales
        //    DropBoxStorageProviderSession dropBoxSession = session as DropBoxStorageProviderSession;

        //    // build the path for resource
        //    String resourcePath = GenericHelper.GetResourcePath(fileSystemEntry);

        //    // trim heading and trailing slashes
        //    resourcePath = resourcePath.TrimStart('/');
        //    resourcePath = resourcePath.TrimEnd('/');

        //    // build the metadata url
        //    String getMetaData;

        //    // get url
        //    if (dropBoxSession.SandBoxMode)
        //        getMetaData = PathHelper.Combine(GetUrlString(DropBoxSandboxRoot, session.ServiceConfiguration), HttpUtilityEx.UrlEncodeUTF8(resourcePath));
        //    else
        //        getMetaData = PathHelper.Combine(GetUrlString(DropBoxDropBoxRoot, session.ServiceConfiguration), HttpUtilityEx.UrlEncodeUTF8(resourcePath));

        //    return getMetaData;
        //}

        //public static String GetDownloadFileUrlInternal(IStorageProviderSession session, ICloudFileSystemEntry entry)
        //{
        //    // cast varibales
        //    DropBoxStorageProviderSession dropBoxSession = session as DropBoxStorageProviderSession;

        //    // gather information
        //    String rootToken = GetRootToken(dropBoxSession);
        //    String dropboxPath = GenericHelper.GetResourcePath(entry);

        //    // add all information to url;
        //    String url = GetUrlString(DropBoxUploadDownloadFile, session.ServiceConfiguration) + "/" + rootToken;

        //    if (dropboxPath.Length > 0 && dropboxPath[0] != '/')
        //        url += "/";

        //    url += HttpUtilityEx.UrlEncodeUTF8(dropboxPath);

        //    return url;
        //}

        #endregion


    }
}
