﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AppLimit.CloudComputing.SharpBox.Common.Net.oAuth.Context;
using AppLimit.CloudComputing.SharpBox.StorageProvider.API;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Logic
{
    /// <summary>
    /// This class implements the behaviour of an authenticted session 
    /// and allows the logic code to get access to the session token, 
    /// the associated service and the service configuration
    /// 
    /// The MockSessionProvider is reponsable to hold our virtual filesystem
    /// which based on the internal object model of SharpBox
    /// 
    /// - Virtual FileSystem is /
    ///     - Data
    ///         - TestFolder1
    ///         - TestFolder2
    ///     - Data2
    ///         - TestFolder3
    ///             - File.rnd
    /// </summary>
    internal class GoogleDriveStorageProviderSession: IStorageProviderSession
    {


        /// <summary>
        /// The ctor gets all security information and generates the starting point 
        /// of out virtual filesystem.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="service"></param>
        /// <param name="config"></param>
        public GoogleDriveStorageProviderSession(GoogleDriveTokenStorage token,   GoogleDriveStorageProviderService service, GoogleDriveProviderConfiguration config)
        {
            // get the parameter
            SessionToken = token;
            Service = service;
            ServiceConfiguration = config;
        }


        #region IStorageProviderSession Members

        public ICloudStorageAccessToken SessionToken { get; private set; }

        public IStorageProviderService Service { get; private set; }

        public ICloudStorageConfiguration ServiceConfiguration { get;private set; }

        #endregion
    }
}
