﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model
{
    public class GoogleDriveAuthCode : ICloudStorageAccessToken
    {
        //https://accounts.google.com/o/oauth2/auth
        public String Code { get; set; }
    }
}
