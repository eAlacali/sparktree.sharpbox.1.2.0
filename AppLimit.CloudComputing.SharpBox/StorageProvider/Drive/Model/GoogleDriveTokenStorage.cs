﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class GoogleDriveTokenStorage : ICloudStorageAccessToken
    {

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>
        /// The access token.
        /// </value>
        [JsonProperty(PropertyName = "access_token")]
        public String AccessToken { get; set; }

        /// <summary>
        /// Gets or sets the expires in.
        /// </summary>
        /// <value>
        /// The expires in.
        /// </value>
        [JsonProperty(PropertyName = "expires_in")]
        public Int32 ExpiresIn { get; set; }

        /// <summary>
        /// Gets or sets the type of the token.
        /// </summary>
        /// <value>
        /// The type of the token.
        /// </value>
        [JsonProperty(PropertyName = "token_type")]
        public String TokenType { get; set; }

        /// <summary>
        /// Gets or sets the refresh token.
        /// </summary>
        /// <value>
        /// The refresh token.
        /// </value>
        [JsonProperty(PropertyName = "refresh_token")]
        public String RefreshToken { get; set; }
    }
}
