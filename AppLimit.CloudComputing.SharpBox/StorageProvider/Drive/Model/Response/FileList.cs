﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Resources;
using Newtonsoft.Json;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Response
{
    internal class FileList
    {
        [JsonProperty(PropertyName = "kind")]
        public String Kind { get; set; }

        [JsonProperty(PropertyName = "etag")]
        public String Etag { get; set; }

        [JsonProperty(PropertyName = "selfLink")]
        public String SelfLink { get; set; }

        [JsonProperty(PropertyName = "nextPageToken")]
        public String NextPageToken { get; set; }

        [JsonProperty(PropertyName = "nextLink")]
        public String NextLink { get; set; }

        [JsonProperty(PropertyName = "items")]
        public List<FileModel> Items { get; set; }
    }
}
