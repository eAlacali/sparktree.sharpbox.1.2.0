using System;
using Newtonsoft.Json;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Resources
{
    internal class ChildModel : IBaseModel
    {
        [JsonProperty(PropertyName = "kind")]
        public String Kind { get; set; }

        [JsonProperty(PropertyName = "id")]
        public String Id { get; set; }

        [JsonProperty(PropertyName = "selfLink")]
        public String SelfLink { get; set; }

        [JsonProperty(PropertyName = "childLink")]
        public String ChildLink { get; set; }

    }
}
