﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model
{
    public class GoogleDriveProviderConfiguration : ICloudStorageConfiguration
    {
        /// <summary>
        /// This method generates an instance of the default dropbox configuration
        /// </summary>
        /// <returns>Instance of the DropBoxConfiguration-Class with default settings</returns>
        static public GoogleDriveProviderConfiguration GetStandardConfiguration()
        {
            return new GoogleDriveProviderConfiguration();
        }


        public Uri AuthorizationCodeUrl
        {
            get { return new Uri("https://accounts.google.com/o/oauth2/auth"); }
        }

        public Uri AccessTokenUrl
        {
            get { return new Uri("https://accounts.google.com/o/oauth2/token"); }
        }

        public Uri RefreshTokenUrl
        {
            get { return new Uri("https://accounts.google.com/o/oauth2/token"); }
        }


        /// <summary>
        /// This field contains the callback URL which will be used in the oAuth 
        /// request. Default will be sharpbox.codeplex.com 
        /// </summary>
        public Uri AuthorizationCallBack = new Uri("http://sharpox.codeplex.com");


        /// <summary>
        /// The scope
        /// </summary>
        public String Scope = "https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/drive.readonly https://www.googleapis.com/auth/drive.file";


        #region ICloudStorageConfiguration Members

        public Uri ServiceLocator
        {
            get { return new Uri("mock://localhost"); }
        }

        public bool TrustUnsecureSSLConnections
        {
            get { return false; }
        }

        public CloudStorageLimits Limits
        {
            get { return new CloudStorageLimits(); }
        }

        #endregion
    }
}
