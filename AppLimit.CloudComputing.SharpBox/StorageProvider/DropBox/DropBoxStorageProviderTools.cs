﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using AppLimit.CloudComputing.SharpBox.Common.Net.Web;
using AppLimit.CloudComputing.SharpBox.Common.Net.oAuth.Impl;
using AppLimit.CloudComputing.SharpBox.StorageProvider.DropBox.Logic;
using AppLimit.CloudComputing.SharpBox.Common.Net.oAuth.Token;
using AppLimit.CloudComputing.SharpBox.Common.Net.oAuth;
using AppLimit.CloudComputing.SharpBox.Common.Net.oAuth.Context;
using AppLimit.CloudComputing.SharpBox.StorageProvider.API;
using AppLimit.CloudComputing.SharpBox.Exceptions;
using AppLimit.CloudComputing.SharpBox.Common.Net.Json;
using AppLimit.CloudComputing.SharpBox.StorageProvider.DropBox.Model;
using Newtonsoft.Json.Linq;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.DropBox
{
    /// <summary>
    /// This class contains a couple a tools which will be helpful
    /// when working with dropbox only
    /// </summary>
    static public class DropBoxStorageProviderTools
    {
        private const String DropBoxMobileLogin = DropBoxStorageProviderService.DropBoxBaseUrl + "/0/token";

        /// <summary>
        /// This method retrieves a new request token from the dropbox server
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="ConsumerKey"></param>
        /// <param name="ConsumerSecret"></param>
        /// <returns></returns>
        static public DropBoxRequestToken GetDropBoxRequestToken(DropBoxConfiguration configuration, String ConsumerKey, String ConsumerSecret)
        {
            // build the consumer context
            var consumerContext = new OAuthConsumerContext(ConsumerKey, ConsumerSecret);

            // build up the oauth session
            var serviceContext = new OAuthServiceContext(configuration.RequestTokenUrl.ToString(),
                                                            configuration.AuthorizationTokenUrl.ToString(), configuration.AuthorizationCallBack.ToString(),
                                                            configuration.AccessTokenUrl.ToString());

            // get a request token from the provider      
            OAuthService svc = new OAuthService();
            OAuthToken oauthToken = svc.GetRequestToken(serviceContext, consumerContext);
            return oauthToken != null ? new DropBoxRequestToken(oauthToken) : null;
        }

        /// <summary>
        /// This method builds derived from the request token a valid authorization url which can be used
        /// for web applications
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="DropBoxRequestToken"></param>
        /// <returns></returns>
        static public String GetDropBoxAuthorizationUrl(DropBoxConfiguration configuration, DropBoxRequestToken DropBoxRequestToken)
        {                   
            // build the auth url
            return OAuthUrlGenerator.GenerateAuthorizationUrl(configuration.AuthorizationTokenUrl.ToString(), 
                                                                configuration.AuthorizationCallBack.ToString(),
                                                                DropBoxRequestToken.RealToken);
        }

        /// <summary>
        /// This method is able to exchange the request token into an access token which can be used in 
        /// sharpbox. It is necessary that the user validated the request via authorization url otherwise 
        /// this call wil results in an unauthorized exception!
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="ConsumerKey"></param>
        /// <param name="ConsumerSecret"></param>
        /// <param name="DropBoxRequestToken"></param>
        /// <returns></returns>
        static public ICloudStorageAccessToken ExchangeDropBoxRequestTokenIntoAccessToken(DropBoxConfiguration configuration, String ConsumerKey, String ConsumerSecret, DropBoxRequestToken DropBoxRequestToken)
        {           
            // build the consumer context
            var consumerContext = new OAuthConsumerContext(ConsumerKey, ConsumerSecret);

            // build up the oauth session
            var serviceContext = new OAuthServiceContext(configuration.RequestTokenUrl.ToString(),
                                                            configuration.AuthorizationTokenUrl.ToString(), configuration.AuthorizationCallBack.ToString(),
                                                            configuration.AccessTokenUrl.ToString());
            
            // build the access token
            OAuthService svc = new OAuthService();
            OAuthToken accessToken = svc.GetAccessToken(serviceContext, consumerContext, DropBoxRequestToken.RealToken);
            if (accessToken == null)
                throw new UnauthorizedAccessException();

            // create the access token 
            return new DropBoxToken(    accessToken, 
                                        new DropBoxBaseTokenInformation() 
                                        { 
                                            ConsumerKey = ConsumerKey, 
                                            ConsumerSecret = ConsumerSecret 
                                        });            
        }

        /// <summary>
        /// This method returns the account information of a dropbox account
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        static public DropBoxAccountInfo GetAccountInformation(ICloudStorageAccessToken token)
        {
            // generate the dropbox service
            DropBoxStorageProviderService service = new DropBoxStorageProviderService();

            // generate a session
            IStorageProviderSession session =  service.CreateSession(token, DropBoxConfiguration.GetStandardConfiguration());
            if (session == null)
                return null;

            // receive acc info
            DropBoxAccountInfo accInfo = service.GetAccountInfo(session);
            
            // close the session
            service.CloseSession(session);

            // go ahead
            return accInfo;
        }

        /// <summary>
        /// A way of letting you keep up with changes to files and folders in a user's Dropbox. You can periodically call /delta to get a list of "delta entries", which are instructions on how to update your local state to match the server's state.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <exception cref="SharpBox"></exception>
        /// <exception cref="SharpBoxException"></exception>
        static public DeltaPage GetAccountDelta(ICloudStorageAccessToken token, String syncCursor = null )
        {
            // generate the dropbox service
            DropBoxStorageProviderService service = new DropBoxStorageProviderService();

            // generate a session
            IStorageProviderSession session = service.CreateSession(token, DropBoxConfiguration.GetStandardConfiguration());
            if (session == null)
                throw new SharpBox.Exceptions.SharpBoxException(AppLimit.CloudComputing.SharpBox.Exceptions.SharpBoxErrorCodes.ErrorInvalidParameters);
            try
            {
                int code;
                // https://api.dropbox.com/1/delta
                string path =
                    @"https://api.dropbox.com/1/delta";
                Dictionary<String, String> parameters = new Dictionary<string, string>();
                if(!String.IsNullOrEmpty(syncCursor))
                {
                    parameters.Add("cursor", syncCursor);
                }
                //
                var result = DropBoxRequestParser.RequestResourceByUrl(path, parameters, service, session, out code, WebRequestMethodsEx.Http.Post);

                if (code != 200)
                    throw new SharpBoxException(SharpBoxErrorCodes.ErrorCreateOperationFailed);

                JsonHelper json = new JsonHelper();
                json.ParseJsonMessage(result);

                DeltaPage delta = new DeltaPage();
                delta.Cursor = json.GetProperty("cursor");
                delta.Reset = json.GetBooleanProperty("reset");
                delta.Has_More = json.GetBooleanProperty("has_more");
                delta.Entries = new List<DeltaEntry>();

                var entryList = json.GetListProperty("entries");
                
                foreach(var entry in entryList)
                {
                    try
                    {
                        DeltaEntry deltaEntry = new DeltaEntry();
                        JArray entryArr = JArray.Parse(entry);
                        deltaEntry.Path = Convert.ToString(entryArr.First()).Replace("\"","");
                        if(entryArr[1] != null && entryArr[1].HasValues)
                        {
                            MetaData metaDataEntry = new MetaData();
                            JsonHelper metaDataHelper = new JsonHelper();
                            metaDataHelper.ParseJsonMessage(entryArr[1]);
                            metaDataEntry.Revision = metaDataHelper.GetPropertyInt("revision");
                            metaDataEntry.Rev = metaDataHelper.GetProperty("rev");
                            metaDataEntry.Path = metaDataHelper.GetProperty("path");
                            metaDataEntry.Is_Dir = metaDataHelper.GetBooleanProperty("is_dir");
                            metaDataEntry.Bytes = metaDataHelper.GetPropertyInt("bytes");

                            deltaEntry.MetaData = metaDataEntry;

                        }
                        else
                        {
                            deltaEntry.MetaData = null;
                        }

                        delta.Entries.Add(deltaEntry);

                    }
                    catch(Exception ex)
                    {
                        throw ex;
                    }



                }
                return delta;

                //return new Uri(json.GetProperty("url"));
            }
#if MONOTOUCH || WINDOWS_PHONE || MONODROID
            catch (Exception ex)
            {
                throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotContactStorageService, ex);
            }
#else
            catch (System.Web.HttpException netex)
            {
                throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotContactStorageService, netex);
            }
#endif
            finally
            {
                // close the session
                service.CloseSession(session);
            }
        }

        private static string UpperCaseUrlEncode(string s)
        {
            char[] temp = HttpUtility.UrlEncode(s).ToCharArray();
            for (int i = 0; i < temp.Length - 2; i++)
            {
                if (temp[i] == '%')
                {
                    temp[i + 1] = char.ToUpper(temp[i + 1]);
                    temp[i + 2] = char.ToUpper(temp[i + 2]);
                }
            }

            var values = new Dictionary<string, string>()
    {
        { "+", "%20" },
        { "(", "%28" },
        { ")", "%29" }
    };

            var data = new StringBuilder(new string(temp));
            foreach (string character in values.Keys)
            {
                data.Replace(character, values[character]);
            }
            return data.ToString();
        }


        /// <summary>
        /// This method returns the public URL of a DropBox file or folder
        /// </summary>
        /// <param name="token"></param>
        /// <param name="fsEntry"></param>
        /// <returns></returns>
        static public Uri GetPublicObjectUrl(ICloudStorageAccessToken token, ICloudFileSystemEntry fsEntry)
        {
            // check parameters
            if (fsEntry == null)
                throw new SharpBox.Exceptions.SharpBoxException(AppLimit.CloudComputing.SharpBox.Exceptions.SharpBoxErrorCodes.ErrorInvalidParameters);

            // get the resource path
            String resourcePath = GenericHelper.GetResourcePath(fsEntry);

            // generate the dropbox service
            DropBoxStorageProviderService service = new DropBoxStorageProviderService();

            // generate a session
            IStorageProviderSession session = service.CreateSession(token, DropBoxConfiguration.GetStandardConfiguration());
            if (session == null)
                throw new SharpBox.Exceptions.SharpBoxException(AppLimit.CloudComputing.SharpBox.Exceptions.SharpBoxErrorCodes.ErrorInvalidParameters);
            
            try
            {
                int code;
                // https://api.dropbox.com/1/shares/<root>/<path>
                string path =
                    DropBoxStorageProviderService.GetUrlString(DropBoxStorageProviderService.DropBoxShareItem,
                                                               session.ServiceConfiguration) + "/" +
                    DropBoxStorageProviderService.GetRootToken(session as DropBoxStorageProviderSession) + "/" +
                    UpperCaseUrlEncode(resourcePath.TrimStart('/'));
                Dictionary<String,String> parameters = new Dictionary<string, string>();
                parameters.Add("short_url","false");
                var result = DropBoxRequestParser.RequestResourceByUrl(path, parameters, service, session, out code, WebRequestMethodsEx.Http.Post);

                if (code != 200)
                    throw new SharpBoxException(SharpBoxErrorCodes.ErrorCreateOperationFailed);

                JsonHelper json = new JsonHelper();
                json.ParseJsonMessage(result);

                return new Uri(json.GetProperty("url"));
            }
#if MONOTOUCH || WINDOWS_PHONE || MONODROID
            catch (Exception ex)
            {
                throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotContactStorageService, ex);
            }
#else
            catch (System.Web.HttpException netex)
            {
                throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotContactStorageService, netex);
            }
#endif
            finally
            {
                // close the session
                service.CloseSession(session);
            }
        }
        
        
        /// <summary>
        /// This method offers the mobile login api of dropbox for users who are migrating from version 0 of the 
        /// dropbox API because version 1 supports token based logins only
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="appkey"></param>
        /// <param name="appsecret"></param>
        /// <returns></returns>
        static public ICloudStorageAccessToken LoginWithMobileAPI(String username, String password, String appkey, String appsecret)
        {
            // get the configuration
            DropBoxConfiguration configuration = DropBoxConfiguration.GetStandardConfiguration();

            // build the consumer context
            var consumerContext = new OAuthConsumerContext(appkey, appsecret);

            // build up the oauth session
            var serviceContext = new OAuthServiceContext(configuration.RequestTokenUrl.ToString(),
                configuration.AuthorizationTokenUrl.ToString(), configuration.AuthorizationCallBack.ToString(),
                configuration.AccessTokenUrl.ToString());

            // get a request token from the provider      
            OAuthService svc = new OAuthService();
            var oAuthRequestToken = svc.GetRequestToken(serviceContext, consumerContext);
            if (oAuthRequestToken == null)
            {
                throw new SharpBoxException(SharpBoxErrorCodes.ErrorInvalidConsumerKeySecret);
            }
            DropBoxToken DropBoxRequestToken = new DropBoxToken(oAuthRequestToken, new DropBoxBaseTokenInformation() { ConsumerKey = appkey, ConsumerSecret = appsecret });

            // generate the dropbox service
            DropBoxStorageProviderService service = new DropBoxStorageProviderService();

            // build up a request Token Session
            var requestSession = new DropBoxStorageProviderSession(DropBoxRequestToken, configuration, consumerContext, service);

            // build up the parameters
            var param = new Dictionary<String, String>
            {
            	{ "email", username}, 
				{ "password", password }
            };

            // call the mobile login api 
            String result = "";

            try
            {
                int code;
                result = DropBoxRequestParser.RequestResourceByUrl(DropBoxMobileLogin, param, service, requestSession, out code);
                if (result.Length == 0)
                    throw new UnauthorizedAccessException();
            }

#if MONOTOUCH || WINDOWS_PHONE || MONODROID
            catch (Exception ex)
            {
                if (ex is UnauthorizedAccessException)
                    throw ex;
                else
                    throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotContactStorageService, ex);
            }
#else
            catch (System.Web.HttpException netex)
            {
                throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotContactStorageService, netex);
            }
#endif

            // exchange a request token for an access token
            var accessToken = new DropBoxToken(result);

            // adjust the token 
            if (accessToken.BaseTokenInformation == null)
            {
                accessToken.BaseTokenInformation = new DropBoxBaseTokenInformation();
                accessToken.BaseTokenInformation.ConsumerKey = appkey;
                accessToken.BaseTokenInformation.ConsumerSecret = appsecret;
            }


            // go ahead
            return accessToken;
        }
    }
}
